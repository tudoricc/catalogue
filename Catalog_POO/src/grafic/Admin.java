
package grafic;

import liceu.Centralizator;


public class Admin extends javax.swing.JFrame {
	private javax.swing.JButton Adaugare;
	private javax.swing.JButton Stergere;
	private javax.swing.JMenu jMenu1;
	private javax.swing.JMenuBar jMenuBar1;
	private javax.swing.JTextField users;
	private javax.swing.JList jList1;
	private javax.swing.JLabel Nume;

	public javax.swing.JList getjList1() {
		return jList1;
	}

	public void setjList1(javax.swing.JList jList1) {
		this.jList1 = jList1;
	}
	private javax.swing.JScrollPane jScrollPane1;

	public Admin(){
		initComponents();
	}


	@SuppressWarnings("unchecked")
	// <editor-fold defaultstate="collapsed" desc="Generated Code">                          
	private void initComponents() {

		Adaugare = new javax.swing.JButton();
		Stergere = new javax.swing.JButton();
		jScrollPane1 = new javax.swing.JScrollPane();
		jList1 = new javax.swing.JList();
		jMenuBar1 = new javax.swing.JMenuBar();
		jMenu1 = new javax.swing.JMenu();
		Nume = new javax.swing.JLabel();

		setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

		Adaugare.setText("Adauga");
		Adaugare.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				AdaugareActionPerformed(evt);
			}
		});

		Stergere.setText("Sterge");
		Stergere.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				StergereActionPerformed(evt);
			}
		});

		jList1.setModel(new javax.swing.AbstractListModel() {
			String[] strings = Centralizator.getInstance().get_username();
			public int getSize() { return strings.length; }
			public Object getElementAt(int i) { return strings[i]; }
		});
		jList1.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
			public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
				jList1ValueChanged(evt);
			}
		});
		jScrollPane1.setViewportView(jList1);

		jMenu1.setText("Exit");
		jMenu1.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jMenu1ActionPerformed(evt);
			}
		});
		jMenuBar1.add(jMenu1);

		setJMenuBar(jMenuBar1);

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(
				layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
						.addContainerGap()
						.addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)
						.addGap(47, 47, 47)
						.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
								.addComponent(Adaugare, javax.swing.GroupLayout.DEFAULT_SIZE, 91, Short.MAX_VALUE)
								.addComponent(Stergere, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
								.addContainerGap(364, Short.MAX_VALUE))
								// .addComponent(Nume,javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
				);

		layout.setVerticalGroup(
				layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
						.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
								.addGroup(layout.createSequentialGroup()
										.addGap(77, 77, 77)
										.addComponent(Adaugare, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
										.addGap(33, 33, 33)
										.addComponent(Stergere, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
										.addGroup(layout.createSequentialGroup()
												.addGap(45, 45, 45)
												.addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)))
												.addContainerGap(189, Short.MAX_VALUE))
				);

		pack();
		show();
		// </editor-fold> 

	}// </editor-fold>                        

	private void jMenu1ActionPerformed(java.awt.event.ActionEvent evt) {                                       
		System.exit(0);
		// TODO add your handling code here:
	}                                      

	private void usersActionPerformed(java.awt.event.ActionEvent evt) {                                      
		// TODO add your handling code here:
	}  


	//useri noi
	private void AdaugareActionPerformed(java.awt.event.ActionEvent evt) {                                         
		Register f =new Register(this);


		// TODO add your handling code here:
	}                               


	private void jList1ValueChanged(javax.swing.event.ListSelectionEvent evt) {                                    
		// jList1.getSelectedValue();
		// TODO add your handling code here:
	}  


	//stergerea unui utilizator din centralizator
	private void StergereActionPerformed(java.awt.event.ActionEvent evt) {                                         
		Centralizator.getInstance().get_utilizatori().remove(jList1.getSelectedIndex());

		jList1.setModel(new javax.swing.AbstractListModel() {
			String[] strings = Centralizator.getInstance().get_username();
			public int getSize() { return strings.length; }
			public Object getElementAt(int i) { return strings[i]; }
		});

		//salvez in fisier ce am modificat in centralizator
		Centralizator.getInstance().save();
		// TODO add your handling code here:
	}   





}
