package grafic;
import java.io.*;
import liceu.Centralizator;

public class Main {

	//pagina de main=rulare a interfetei grafice
	//aici am incarcat din fisierul serializat centralizatorul propriu zis

	public static void main(String[] args) {
		Centralizator cent;
		try  
		{  
			File nume = new File("Centralizator.ser");
			if (nume.exists()){
				FileInputStream fileIn =new FileInputStream("Centralizator.ser");  
				ObjectInputStream in = new ObjectInputStream(fileIn);  
				Centralizator.getInstance().set((Centralizator) in.readObject());  
				// System.out.println(Centralizator.getInstance().toString());
				in.close();  
				fileIn.close();  

			}
			else {
				nume.createNewFile();
				System.out.println("Nu exista");
			}
		}catch(IOException i)  
		{  
			i.printStackTrace();  
			return;  
		}catch(ClassNotFoundException c)  
		{  
			// System.out.println("Employee class not found");  
			c.printStackTrace();  
			return;  
		}  

		Intro f = new Intro();
		f.show();
	}
}
