package liceu;
import java.util.*;
interface IProfesor {
	
	//Listarea elevilor unei clase
	void list_clasa(Clasa x);

	//Ordonarea elevilor unei clase după un criteriu specificat
	void sortare(Object o,Clasa x);
	
	//ordonarea elevilor unei clase dupa 
	//adăugare note noi
	void add_note_sem1(Elev x ,Clasa y, int note,Profesor p);
	
	//adaugare note noi sem 2
	void add_note_sem2(Elev x ,Clasa y, int note,Profesor p);
	//modificare (incheiere) medii
	void inch_medii(Elev x,Clasa y,Materie z,Profesor p);
	//incheiere medie semestrul 1 la o materie
	public void inch_medie1(Elev x,Clasa y,Materie z,Profesor p) ;
	//incheiere medie semestrul 2 la o materie
	public void inch_medie2(Elev x,Clasa y,Materie z,Profesor p) ;
	
	//adăugarea unei absențe unui elev pe o anumita data
	void add_abs(Elev x , Clasa y,Data date,Profesor p);
	
	//modificarea statusului unei absențe
	void mod_abs(Clasa x , Elev y ,Absente abs ,String situatie,Profesor p);
}
