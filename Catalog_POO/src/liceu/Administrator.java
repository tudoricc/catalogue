package liceu;

import java.util.*;
import java.io.*;

public class Administrator extends Utilizator implements IAdministrator  {
	
	//constructor pentru administratorul implicit
	public Administrator(){
		super();
	}
	
	//metode din interfata Iadministrator
	
	//adaugarea unui utilizator prin Factory Pattern in functie de String-ul pe care i-l parsezi
	@Override
	public boolean add(Object o,String id) throws IOException {
		// TODO Auto-generated method stub
		if (o instanceof Utilizator){
			Utilizator user  = (Utilizator)o;
			FactPatt fp = FactPatt.getInstance();
			if (id.equals("Elev")){
				Elev elev = (Elev)fp.getUtilizator("Elev");
				elev.set_nume(user.get_numeutilizator());
				elev.set_numeutilizator(user.get_nume());
				elev.set_parola(user.get_parola());
				elev.set_rol("Elev");
				
				Centralizator.getInstance().add(elev);
				
			}
			else if (id.equals("Profesor")){
				Profesor prof = (Profesor) fp.getUtilizator("Profesor");
				prof.set_nume(user.get_numeutilizator());
				prof.set_numeutilizator(user.get_nume());
				prof.set_parola(user.get_parola());
				prof.set_rol("Profesor");
				
				Centralizator.getInstance().add(prof);
			}
			else if (id.equals("Secretar")){
				
				Secretar secretar = (Secretar) fp.getUtilizator("Secretar");
				secretar.set_nume(user.get_numeutilizator());
				secretar.set_numeutilizator(user.get_nume());
				secretar.set_parola(user.get_parola());
				secretar.set_rol("Elev");
				ArrayList<Utilizator> users = Centralizator.getInstance().get_utilizatori();
				users.add(secretar);
				
				Centralizator.getInstance().set_utilizatori(users);
			}
		}
		return false;
	}

	//stergerea unui Utilizator din lista de utilizatori
	@Override
	public void delete(Object o)throws IOException {
		// TODO Auto-generated method stub
		ArrayList<Utilizator>users = Centralizator.getInstance().get_utilizatori();
		users.remove(o);
		
		Centralizator.getInstance().set_utilizatori(users);
		
	}
	
	//listarea tuturor utilizatorilor din centralizator
	@Override
	public void list() {
		// TODO Auto-generated method stub
		System.out.println(Centralizator.getInstance().get_utilizatori().toString());
		
	}
	
	//metode din clasa Secretar 
	
	//adaugarea unei clase
	@Override
	public void add_clasa(Object o) {
		// TODO Auto-generated method stub
		if (o instanceof Clasa){
			Clasa clasa = (Clasa) o;
			ArrayList<Clasa>clase =Centralizator.getInstance().get_clase();
			clase.add(clasa);
			Centralizator.getInstance().set_clase(clase);
			Centralizator.getInstance().save();
			
			
		}
		
	}
	
	//stergerea unei clase
	@Override
	public void del_clasa(Object o) {
		// TODO Auto-generated method stub
		if (o instanceof Clasa){
			Clasa clasa = (Clasa )o;
			ArrayList<Clasa> clase =Centralizator.getInstance().get_clase();
			clase.remove(clasa);
			Centralizator.getInstance().set_clase(clase);
			//	Centralizator.getInstance().get_centralizator().values().remove(clasa);
		}
	}

	//editarea ID-ului unei clase
	@Override
	public void edit_clasa_ID(Object o,String ID_nou) {
		// TODO Auto-generated method stub
		if (o instanceof Clasa ){
			Clasa copie = (Clasa) o;
			if (Centralizator.getInstance().get_clase().contains(copie)){
				int index =Centralizator.getInstance().get_clase().indexOf(copie);
				ArrayList<Clasa> clase = Centralizator.getInstance().get_clase();
				clase.get(index).set_idClasa(ID_nou);
				
			}
		}
		
	}

	
	//editarea numelui unei materii pentru un profesor
	@Override
	public void edit_materie_p_nume(Object o,Profesor p,String nou) {
		// TODO Auto-generated method stub
		if (o instanceof Materie){
			Materie copie = (Materie) o;
			if (Centralizator.getInstance().get_centralizator().containsKey(copie)){
				if(Centralizator.getInstance().get_centralizator().get(copie).values().contains(p)){
					
					Materie copie2=copie;
					Centralizator.getInstance().get_centralizator().get(copie).values().remove(p);
					copie.setNume(nou);
					p.set_mat(copie);
					Centralizator.getInstance().get_centralizator().get(copie).values().add(p);
					Hashtable<Clasa,Profesor> tabel= Centralizator.getInstance().get_centralizator().get(copie2);
					Centralizator.getInstance().get_centralizator().put(copie, tabel);
				}
			}
		}
	}
	
	//editarea numarului de ore pentru o materie a unui profesor
	@Override
	public void edit_materie_p_nrore(Object o, Profesor p,int nr_ore) {
		// TODO Auto-generated method stub
		if (o instanceof Materie){
			Materie copie = (Materie) o;
			if (Centralizator.getInstance().get_centralizator().containsKey(copie)){
				if(Centralizator.getInstance().get_centralizator().get(copie).values().contains(p)){
					Materie copie2=copie;
					Centralizator.getInstance().get_centralizator().get(copie).values().remove(p);
					copie.set_nrore(nr_ore);
					p.set_mat(copie);
					Centralizator.getInstance().get_centralizator().get(copie).values().add(p);
					Hashtable<Clasa,Profesor> tabel= Centralizator.getInstance().get_centralizator().get(copie2);
					Centralizator.getInstance().get_centralizator().put(copie, tabel);
				}
			}
		}
	}
	
	//editarea situatiei la teza pentru un profeosr la o materie
	@Override
	public void edit_materie_p_teza(Object o, Profesor p,boolean sit) {
		// TODO Auto-generated method stub
		if (o instanceof Materie){
			Materie copie = (Materie) o;
			if (Centralizator.getInstance().get_centralizator().containsKey(copie)){
				if(Centralizator.getInstance().get_centralizator().get(copie).values().contains(p)){
					Materie copie2=copie;
					Centralizator.getInstance().get_centralizator().get(copie).values().remove(p);
					copie.set_teza(sit);
					p.set_mat(copie);
					Centralizator.getInstance().get_centralizator().get(copie).values().add(p);
					Hashtable<Clasa,Profesor> tabel= Centralizator.getInstance().get_centralizator().get(copie2);
					Centralizator.getInstance().get_centralizator().put(copie, tabel);
				}
			}
		}
	}
	
	//stergerea unei materii pentru un profesor
	@Override
	public void del_materie_p(Object o,Profesor p) {
		// TODO Auto-generated method stub
		if (o instanceof Materie){
			Materie copie = (Materie) o;
			if(p.get_mat()==copie){
				p.set_mat(null);
				Centralizator.getInstance().get_centralizator().keySet().remove(copie);
			}
	}
	}
	
	//adaugarea unei materii la un profesor
	@Override
	public void add_materie_p(Object o,Profesor p) {
		// TODO Auto-generated method stub
		if(o instanceof Materie){
			Materie copie = (Materie) o;
			
			if (Centralizator.getInstance().get_centralizator().containsKey(copie)){
				Centralizator.getInstance().get_centralizator().get(copie).values().remove(p);
				p.set_mat(copie);
				Centralizator.getInstance().get_centralizator().get(copie).values().add(p);
				
			}
			
			
		}
	}

	//adaugarea materie la o clasa
	@Override
	public void add_materie_c(Object o, Clasa y) {
		// TODO Auto-generated method stub
		if (o instanceof Materie){
			Materie mat= (Materie) o;
			if(Centralizator.getInstance().get_clase().contains(y)){
				ArrayList<Clasa> clase = Centralizator.getInstance().get_clase();
				int index = Centralizator.getInstance().get_clase().indexOf(y);
				ArrayList<Materie> materii= clase.get(index).get_materii();
				materii.add(mat);
				clase.get(index).set_materii(materii);
				Centralizator.getInstance().set_clase(clase);
			}
		}
	}

	//stergerea unei materii dintr-o clasa
	@Override
	public void del_materie_c(Object o, Clasa y) {
		// TODO Auto-generated method stub
		if (o instanceof Materie){
			Materie mat = (Materie) o;
			if(Centralizator.getInstance().get_clase().contains(y)){
				ArrayList<Clasa> clase=Centralizator.getInstance().get_clase();
				int index = Centralizator.getInstance().get_clase().indexOf(y);
				ArrayList<Materie> materii= clase.get(index).get_materii();
				materii.remove(mat);
				clase.get(index).set_materii(materii);
				Centralizator.getInstance().set_clase(clase);
			}
		}
	}

	//editarea numelui unei materii dintr-o clasa
	@Override
	public void edit_materie_c_nume(Object o,Clasa y,String nume){
		// TODO Auto-generated method stub
		if(o instanceof Materie){
			if (Centralizator.getInstance().get_clase().contains(y)){
				Materie mat = (Materie) o;
				Materie mat2=mat;
				ArrayList<Clasa> clase = Centralizator.getInstance().get_clase();
				int index = Centralizator.getInstance().get_clase().indexOf(y);
				ArrayList<Materie> materii= clase.get(index).get_materii();
				int index2 = materii.indexOf(mat);
				materii.get(index2).setNume(nume);
				clase.get(index).set_materii(materii);
				Centralizator.getInstance().set_clase(clase);
			}

		}
		}
	
	//editarea numarului de ore pentru o materie dintr-o clasa
	@Override
	public void edit_materie_c_nrore(Object o, Clasa y,int nr_ore) {
		// TODO Auto-generated method stub
		if(o instanceof Materie){
			if (Centralizator.getInstance().get_clase().contains(y)){
				Materie mat = (Materie) o;
				Materie mat2=mat;
				ArrayList<Clasa> clase = Centralizator.getInstance().get_clase();
				int index = Centralizator.getInstance().get_clase().indexOf(y);
				ArrayList<Materie> materii= clase.get(index).get_materii();
				int index2 = materii.indexOf(mat);
				materii.get(index2).set_nrore(nr_ore);
				clase.get(index).set_materii(materii);
				Centralizator.getInstance().set_clase(clase);
			}

		}
	}
	
	//aduagarea unui elev la o clasa
	@Override
	public void add_elev_c(Object o, Clasa y) {
		// TODO Auto-generated method stub
		if (o instanceof Elev){
			Elev elev = (Elev) o;
			Clasa copie = y;
			if(Centralizator.getInstance().get_clase().contains(y)){
				ArrayList<Clasa> clase = Centralizator.getInstance().get_clase();
				int index = Centralizator.getInstance().get_clase().indexOf(y);
				clase.get(index).add_elev(elev);
				Centralizator.getInstance().set_clase(clase);
			}
		}
	}

	
	//Stergerea unui elev dintr-o clasa
	@Override
	public void del_elev_c(Object o, Clasa y) {
		// TODO Auto-generated method stub
		if ( o instanceof Elev){
			Elev elev = (Elev) o;
			if(Centralizator.getInstance().get_clase().contains(y)){
				ArrayList<Clasa> clase = Centralizator.getInstance().get_clase();
				int index = Centralizator.getInstance().get_clase().indexOf(y);
				clase.get(index).del_elev(elev);
				Centralizator.getInstance().set_clase(clase);
			}
		}
	}

	//editarea unui elev dintr-o clasa
	@Override
	public void edit_elev_c_nume(Object o, Clasa y,String nume) {
		// TODO Auto-generated method stub
		if (o instanceof Elev){
			Elev elev = (Elev) o;
			Clasa copie = y;
			if (Centralizator.getInstance().get_clase().contains(y)){
					ArrayList<Clasa> clase = Centralizator.getInstance().get_clase();
					int index = clase.indexOf(y);
					if (clase.get(index).get_elevi().contains(elev)){
						ArrayList<Elev> elevi = clase.get(index).get_elevi();
						int index2= elevi.indexOf(elev);
						elevi.get(index2).set_nume(nume);
						clase.get(index).set_elevi(elevi);
						Centralizator.getInstance().set_clase(clase);
					}
				}
			
		}
	}
	
	//editarea prenumeului unui elev dintr-0o clasa
	@Override
	public void edit_elev_c_prenume(Object o,Clasa y,String prenume){
		if (o instanceof Elev){
			Elev elev = (Elev) o;
			Clasa copie = y;
			if (Centralizator.getInstance().get_clase().contains(y)){
					ArrayList<Clasa> clase = Centralizator.getInstance().get_clase();
					int index = clase.indexOf(y);
					if (clase.get(index).get_elevi().contains(elev)){
						ArrayList<Elev> elevi = clase.get(index).get_elevi();
						int index2= elevi.indexOf(elev);
						elevi.get(index2).set_prenume(prenume);
						clase.get(index).set_elevi(elevi);
						Centralizator.getInstance().set_clase(clase);
					}
				}
			
		}
	}
	
	//editarea dateri de nastere a unui elev dintr-o clasa
	@Override
	public void edit_elev_c_datanastere(Object o, Clasa y,Data data) {
		// TODO Auto-generated method stub
		if (o instanceof Elev){
			Elev elev = (Elev) o;
			Clasa copie = y;
			if (Centralizator.getInstance().get_clase().contains(y)){
					ArrayList<Clasa> clase = Centralizator.getInstance().get_clase();
					int index = clase.indexOf(y);
					if (clase.get(index).get_elevi().contains(elev)){
						ArrayList<Elev> elevi = clase.get(index).get_elevi();
						int index2= elevi.indexOf(elev);
						elevi.get(index2).set_data(data);
						clase.get(index).set_elevi(elevi);
						Centralizator.getInstance().set_clase(clase);
					}
				}
			
		}
		
	}
	
	//calcularea mediei generale a unui elev dintr-o clasa
	@Override
	public void calc_mg(Object o,Clasa y) {
		// TODO Auto-generated method stub
		if (o instanceof Elev){
			Elev elev=(Elev) o;
			int index = Centralizator.getInstance().get_clase().indexOf(y);
			if (y.get_elevi().contains(elev)){
				Hashtable<Materie,SituatieMaterieBaza> tabel =y.get_Catalog().get_catalog().get(elev);
				double suma=0;
				while (tabel.values().iterator().hasNext()){
					suma+=tabel.values().iterator().next().calc_medie_finala();
				}
				double medie = suma/tabel.keySet().size();
				System.out.println("Media generala a elevui\r\n" + elev.toString() + " este :\r\n" + medie);
			
			}
		}
	}

	//metode de la profeosr
	
	//listarea unei clase
	@Override
	public void list_clasa(Clasa x) {
		// TODO Auto-generated method stub
		int index = Centralizator.getInstance().get_clase().indexOf(x);
		for (Elev e :Centralizator.getInstance().get_clase().get(index).get_elevi()){
			System.out.println( e.toString()) ;
		}
	}
	
	//sortarea unei clase in functie de ceva
	@Override
	public void sortare(Object o,Clasa x) {
		// TODO Auto-generated method stub
		if (o.equals("Nume")){
			ArrayList<Elev> copie = new ArrayList<Elev>();
			copie=x.get_elevi();
			class NameCompare implements Comparator<Elev>{

				@Override
				public int compare(Elev o1, Elev o2) {
					// TODO Auto-generated method stub
					String nume1=o1.get_nume();
					String nume2=o2.get_nume();
					return nume1.compareToIgnoreCase(nume2);
				}
			}
			Collections.sort(copie,new NameCompare());
			copie.toString();
		}
		else if (o.equals("Prenume")){
			ArrayList<Elev> copie = new ArrayList<Elev>();
			copie=x.get_elevi();
			class SurnameCompare implements Comparator<Elev>{

				@Override
				public int compare(Elev o1, Elev o2) {
					// TODO Auto-generated method stub
					String nume1=o1.get_prenume();
					String nume2=o2.get_prenume();
					return nume1.compareToIgnoreCase(nume2);
				}
			}
			Collections.sort(copie,new SurnameCompare());
			copie.toString();
		}
		else if (o.equals("CNP")){
			ArrayList<Elev> copie = new ArrayList<Elev>();
			copie=x.get_elevi();
			class CNPCompare implements Comparator<Elev>{

				@Override
				public int compare(Elev o1, Elev o2) {
					// TODO Auto-generated method stub
					String nume1=o1.get_cnp();
					String nume2=o2.get_cnp();
					return nume1.compareToIgnoreCase(nume2);
				}
			}
			Collections.sort(copie,new CNPCompare());
			copie.toString();
			
		}
		else if (o.equals ("Ziua nastere")){
			ArrayList<Elev> copie = new ArrayList<Elev>();
			copie=x.get_elevi();
			class DayCompare implements Comparator<Elev>{

				@Override
				public int compare(Elev o1, Elev o2) {
					// TODO Auto-generated method stub
					int nume1=o1.get_data().get_day();
					int nume2=o2.get_data().get_day();
					if (nume1<nume2){
						return -1;
						
					}
					else return 1;
				}
			}
			Collections.sort(copie,new DayCompare());
			copie.toString();
		}
		else if (o.equals ("Luna nastere")){
			ArrayList<Elev> copie = new ArrayList<Elev>();
			copie=x.get_elevi();
			class MonthCompare implements Comparator<Elev>{

				@Override
				public int compare(Elev o1, Elev o2) {
					// TODO Auto-generated method stub
					int nume1=o1.get_data().get_month();
					int nume2=o2.get_data().get_month();
					if (nume1<nume2){
						return -1;
						
					}
					else return 1;
				}
			}
			Collections.sort(copie,new MonthCompare());
			copie.toString();
		}
		else if(o.equals("Anul nasterii")){
			ArrayList<Elev> copie = new ArrayList<Elev>();
			copie=x.get_elevi();
			class YearCompare implements Comparator<Elev>{

				@Override
				public int compare(Elev o1, Elev o2) {
					// TODO Auto-generated method stub
					int nume1=o1.get_data().get_year();
					int nume2=o2.get_data().get_year();
					if (nume1<nume2){
						return -1;
						
					}
					else return 1;
				}
			}
			Collections.sort(copie,new YearCompare());
			copie.toString();
		}
		}
	
	//adaugarea note pe semestrul 1
	@Override
	public void add_note_sem1(Elev x,Clasa y, int nota,Profesor p) {
		// TODO Auto-generated method stub
		
		//deschidere fisier serializare 
		if (Centralizator.getInstance().get_clase().contains(y)){

			int index=Centralizator.getInstance().get_clase().indexOf(y);
			if (y.get_elevi().contains(x)){
				
				Centralizator.getInstance().get_cat_clasa_elev(index, x).get(p.get_mat()).add_nota_sem1(nota);
			}
			else{
				System.out.println("ai gresit ceva");
			}
		}
		//isalvare fisier
	}
	
	//adaugare note pe semestrul 2
	@Override
	public	void add_note_sem2(Elev x ,Clasa y, int nota,Profesor p){
		if (Centralizator.getInstance().get_clase().contains(y)){
			if (y.get_elevi().contains(x)){
				int index = Centralizator.getInstance().get_clase().indexOf(y);
				Centralizator.getInstance().get_cat_clasa_elev(index, x).get(p.get_mat()).add_nota_sem2(nota);
			}
			else{
				System.out.println("ai gresit ceva");
			}
		}
	}
	
	
	//incheiere medie pe sem 1 pentru un profeosr
	public void inch_medie1_prof(Elev x,Clasa y,Profesor p) {
		// TODO Auto-generated method stub
		int index = Centralizator.getInstance().get_clase().indexOf(y);
		if (Centralizator.getInstance().get_cat_clasa(index).containsKey(x)){
			Centralizator.getInstance().get_cat_clasa_elev(index, x).get(p.get_mat()).set_medie_sem1();
	}
	}

	//incheiere medie semestrul 2 pentru un profesor
	public void inch_medie2_prof(Elev x,Clasa y,Profesor p) {
		// TODO Auto-generated method stub
		int index = Centralizator.getInstance().get_clase().indexOf(y);
		if (Centralizator.getInstance().get_cat_clasa(index).containsKey(x)){
			Centralizator.getInstance().get_cat_clasa_elev(index, x).get(p.get_mat()).set_medie_sem2();
			
		}
		
	}
	
	//adaugarea unei absente unui elev pentru un profeosr
	@Override
	public void add_abs(Elev x, Clasa y,Data date,Profesor p) {
		// TODO Auto-generated method stub
		if (y.get_elevi().contains(x)){
		//	y.get_Catalog().get_catalog().
		}
		
	}
	
	//modificarea statusului unei absente pentru un profesor
	@Override
	public void mod_abs(Clasa x, Elev y, Absente abs, String situatie,Profesor p) {
		// TODO Auto-generated method stub
		Absente absenta=new Absente();
		absenta.set_date(abs.get_date());
		absenta.set_situatie(situatie);
		int index1 = Centralizator.getInstance().get_clase().indexOf(x);
		if (Centralizator.getInstance().get_clase().get(index1).get_elevi().contains(y)){
			if (Centralizator.getInstance().get_clase().get(index1).get_Catalog().get_catalog().get(y).get(p.get_mat()).get_absente().contains(abs)){
				int index=Centralizator.getInstance().get_cat_clasa_elev(index1,y).get(p.get_mat()).get_absente().indexOf(abs);
				Centralizator.getInstance().get_cat_clasa_elev(index1, y).get(p.get_mat()).get_absente().get(index).set_date(absenta.get_date());
				Centralizator.getInstance().get_cat_clasa_elev(index1, y).get(p.get_mat()).get_absente().get(index).set_situatie(situatie);
			}
		}
	}
	
	//incheiere medie pe ambele semestre
	@Override
	public void inch_medii(Elev x, Clasa y, Materie z,Profesor p) {
		// TODO Auto-generated method stub
		int index = Centralizator.getInstance().get_clase().indexOf(y);
		
		if (Centralizator.getInstance().get_clase().get(index).get_Catalog().get_catalog().containsKey(x)){
			Centralizator.getInstance().get_cat_clasa_elev(index, x).get(z).set_medie_sem1();
			Centralizator.getInstance().get_cat_clasa_elev(index,x).get(z).set_medie_sem2();
			
		}
		
	}
	
	//incheiere medie pe semestrul 1
	@Override
	public void inch_medie1(Elev x, Clasa y, Materie z,Profesor p) {
		// TODO Auto-generated method stub
		int index=Centralizator.getInstance().get_clase().indexOf(y);
		if (Centralizator.getInstance().get_cat_clasa(index).containsKey(x)){
			Centralizator.getInstance().get_cat_clasa_elev(index, x).get(z).set_medie_sem1();		
			
		}
		
	}
	
	//incheiere medie pe semestrul 2
	@Override
	public void inch_medie2(Elev x, Clasa y, Materie z,Profesor p) {
		// TODO Auto-generated method stub
		int index=Centralizator.getInstance().get_clase().indexOf(y);
		if (Centralizator.getInstance().get_cat_clasa(index).containsKey(x)){
			Centralizator.getInstance().get_cat_clasa_elev(index, x).get(p.get_mat()).set_medie_sem2();
			
		}
	}
	
	
	}

	

