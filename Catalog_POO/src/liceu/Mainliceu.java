package liceu;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.*;

import liceu.SituatieMaterieBaza.Absenta;
public class Mainliceu {
	
	public static void main(String[] args) throws IOException{
		//O functie main cu cateva exemple.
		//am pus si serializarea dar am niste exemple cam urate in fisier de care nu pot scapa
		//pentru ca atunci cand am creat anumite clase din interfata am uitat sa le pun un id.
		/*try  
	       {  
				File nume = new File("Centralizator.ser");
	          if (nume.exists()){
	        	  FileInputStream fileIn =new FileInputStream("Centralizator.ser");  
	        	  ObjectInputStream in = new ObjectInputStream(fileIn);  
	        	  Centralizator.getInstance().set((Centralizator) in.readObject());  
	        	 // System.out.println(Centralizator.getInstance().toString());
	        	  in.close();  
	        	  fileIn.close();  
	          
	          }
	          else {
	        	  nume.createNewFile();
	        	  System.out.println("Nu exista");
	          }
	       }catch(IOException i)  
	       {  
	          i.printStackTrace();  
	          return;  
	       }catch(ClassNotFoundException c)  
	       {  
	         // System.out.println("Employee class not found");  
	          c.printStackTrace();  
	          return;  
	       }  
		*/
		Materie a = new Materie("Biologie",4,false);
		Materie fiz = new Materie("Fizica ",6,true);
		Materie mate = new Materie("Mate",5,false);
		Data data = new Data(15,12,1993);
		Profesor prof = new Profesor("vasile","tudor","Vasile","Ion","profeosr",mate);
		Elev tudor = new Elev("tudor","tudor","tudor","tudor","elev",data);
		Elev marius = new Elev("marius","marius","marius","marius","elev",data);
		Elev cosmin = new Elev("cosmin","cosmin","cosmin","cosmin","elev",data);
		Elev alexandra = new Elev("alexandra","alexandra","alexandra","alexandra","elev",data);
		Elev lucian = new Elev("lucian","lucian","lucian","lucian","elev",data);
		Elev alexandru = new Elev("alex","alex","alex","alex","elev",data);
		
		Clasa unu= new Clasa();
		unu.set_idClasa("9B");
		unu.add_elev(tudor);
		unu.add_elev(marius);
		unu.add_elev(cosmin);
		
		SituatieMaterieBaza sit1 = new SituatieMaterieBaza();
		sit1.add_nota_sem1(4);
		sit1.add_nota_sem1(7);
		sit1.add_nota_sem1(6);
		sit1.add_nota_sem2(2);
		sit1.add_nota_sem2(6);
		sit1.add_nota_sem2(4);
		Absente ab1= new Absente();
		ab1.set_date(data);
		ab1.set_situatie("motivata");
		sit1.set_absente(new ArrayList());
		
		SituatieMaterieBaza sit2 = new SituatieMaterieBaza();
		sit2.add_nota_sem1(4);
		sit2.add_nota_sem1(5);
		sit2.add_nota_sem1(1);
		sit2.add_nota_sem2(4);
		sit2.add_nota_sem2(6);
		sit2.add_nota_sem2(8);
		sit2.set_absente(new ArrayList());
		
		sit1.set_nume_materie(mate);
		sit2.set_nume_materie(fiz);
		
		Hashtable<Materie,SituatieMaterieBaza> cat = new Hashtable<Materie, SituatieMaterieBaza>();
		cat.put(mate, sit1);
		cat.put(fiz, sit2);
		unu.add_elev_special(tudor, cat);
		
		SituatieMaterieBaza sit11 = new SituatieMaterieBaza();
		sit11.add_nota_sem1(4);
		sit11.add_nota_sem1(2);
		sit11.add_nota_sem1(5);
		sit11.add_nota_sem2(2);
		sit11.add_nota_sem2(6);
		sit11.add_nota_sem2(4);
		
		sit11.set_absente(new ArrayList());
		
		SituatieMaterieBaza sit22 = new SituatieMaterieBaza();
		sit22.add_nota_sem1(4);
		sit22.add_nota_sem1(5);
		sit22.add_nota_sem1(1);
		sit22.add_nota_sem2(4);
		sit22.add_nota_sem2(6);
		sit22.add_nota_sem2(8);
		sit22.set_absente(new ArrayList());
		
		sit11.set_nume_materie(mate);
		sit22.set_nume_materie(fiz);
		
		Hashtable<Materie,SituatieMaterieBaza> cat2 = new Hashtable<Materie, SituatieMaterieBaza>();
		cat2.put(mate, sit11);
		cat2.put(fiz, sit22);
		unu.add_elev_special(marius, cat);
		
		Clasa doi = new Clasa();
		doi.set_idClasa("Sefii");
		doi.add_elev(alexandra);
		
		ArrayList<Clasa> clase = new ArrayList<Clasa>();
		clase.add(unu);
		clase.add(doi);
		ArrayList<Utilizator> users = new ArrayList<Utilizator>();
		users.add(tudor);
		users.add(alexandru);
		users.add(cosmin);
		users.add(marius);
		users.add(alexandra);
		
		Clasa trei = new Clasa();
		trei.set_idClasa("mere");
		trei.add_elev(alexandra);
		
		Secretar secretar = new Secretar();
		secretar.add_clasa(trei);
		secretar.add_elev_c(tudor, trei);
		//secretar.del_elev_c(tudor,trei);
		//secretar.del_clasa(trei);
		secretar.add_materie_c(fiz, trei);
		secretar.add_materie_c(mate, trei);
		secretar.edit_materie_c_nume(fiz, trei, "putulica");
		secretar.edit_materie_c_nrore(fiz, trei, 123213312);
		secretar.add_elev_c(cosmin, trei);
		secretar.del_elev_c(cosmin, trei);
		secretar.edit_elev_c_nume(tudor,trei,"vasilica");
		secretar.edit_clasa_ID(trei, "IUBIII");
		//secretar.del_materie_c(fiz, trei);
		Administrator admin = new Administrator();
		//admin.add(tudor,"elev");
		
		admin.add_clasa(unu);

		admin.add_clasa(doi);
		
		//admin.add_clasa(trei);
		// Hashtable<Materie,Hashtable<Clasa,Profesor>>
		
		//Centralizator.getInstance().set_clase(clase);
		Centralizator.getInstance().set_utilizatori(users);
		Hashtable<Clasa,Profesor> tabel = new Hashtable<Clasa,Profesor>();
		tabel.put(unu, prof);
		tabel.put(doi, prof);
		Hashtable<Materie,Hashtable<Clasa,Profesor>> tabel2= new Hashtable<Materie,Hashtable<Clasa,Profesor>>();
		tabel2.put(mate, tabel);
		
		Centralizator.getInstance().set_centralizator(tabel2);
		int index2 = Centralizator.getInstance().get_clase().indexOf(trei);
		System.out.println(Centralizator.getInstance().get_clase().get(index2).toString());
		//prof.add_note_sem2(tudor, unu, 1015515151, prof);
		prof.inch_medie1(tudor, unu, prof.get_mat(), prof);
		prof.inch_medie2(tudor, unu, prof.get_mat(), prof);
		//prof.list_clasa(unu);
		int index=Centralizator.getInstance().get_clase().indexOf(unu);
		//System.out.println(Centralizator.getInstance().get_clase().get(index).get_Catalog().get_catalog().get(tudor));
		//tudor.list_sitsc(unu);
		ArrayList<Materie> lista = new ArrayList<Materie>();
		lista.add(mate);
		lista.add(fiz);
		//prof.sortare("Prenume", unu);
		unu.set_materii(lista);
			
		
		
		
	}
	
	
}
