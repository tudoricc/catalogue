package liceu;

import java.io.Serializable;

public class Materie implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String nume;
	private int nr_ore;
	private boolean teza;
	
	//constructorii
	public Materie(){
		this.nume=" ";
		this.nr_ore=0;
		this.teza=false;
	}
	public Materie(String nume,int nr , boolean teza){
		this.nume=nume;
		this.nr_ore=nr;
		this.teza=teza;
		
	}
	
	//getter si setter pentru nume;
	public String get_nume(){
		return this.nume;
	}
	public void setNume(String nume){
		this.nume=nume;
	}
	
	//getter si setter pentru numarul de ore
	public int get_nrore(){
		return this.nr_ore;
	}
	public void set_nrore(int x){
		this.nr_ore=x;
	}
	
	//getter si setter pentru teza
	public boolean get_teza(){
		return this.teza;
	}
	public void set_teza(boolean situatie){
		this.teza=situatie;
	}
	
	//metoda toString() pentru clasa Materie
	public String toString(){
		String s ="";
		s= " \r\n Numele materiei: \r\n"+this.nume +"\r\n Numarul de ore: \r\n" + this.nr_ore +"\r\n Situatie teza: \r\n"+this.teza+"\r\n";
		return s;
		
	}
}
