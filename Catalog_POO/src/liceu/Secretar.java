package liceu;
import java.util.*;
public class Secretar extends Utilizator implements ISecretar {
	//constructorul implicit
	public Secretar(){
		super();
	}
	
	//constructorul pentru nume,prenume etc
	public Secretar(String nume,String prenume,String username,String parola,String rol){
		this.set_nume(nume);
		this.set_numeutilizator(username);
		this.set_parola(parola);
		this.set_prenume(prenume);
		this.set_rol(rol);
	}
	
	//un secretar pornind de la un utilizator
	public Secretar(Utilizator x){
		this.set_nume(x.get_nume());
		this.set_numeutilizator(x.get_numeutilizator());
		this.set_parola(x.get_parola());
		this.set_prenume(x.get_prenume());
	}
	
	//metode din Interfata Secretar
	
	//adaugarea unei clase la centralizator
	@Override
	public void add_clasa(Object o) {
		// TODO Auto-generated method stub
		if (o instanceof Clasa){
			Clasa clasa = (Clasa) o;
			ArrayList<Clasa>clase =Centralizator.getInstance().get_clase();
			clase.add(clasa);
			Centralizator.getInstance().set_clase(clase);
			Centralizator.getInstance().save();
			
			
		}
		
	}
	
	//Stergere unei clase din centralizator
	@Override
	public void del_clasa(Object o) {
		// TODO Auto-generated method stub
		if (o instanceof Clasa){
			Clasa clasa = (Clasa )o;
			ArrayList<Clasa> clase =Centralizator.getInstance().get_clase();
			clase.remove(clasa);
			Centralizator.getInstance().set_clase(clase);
			//	Centralizator.getInstance().get_centralizator().values().remove(clasa);
		}
	}

	//editare id-ului unei clase din centralizator
	@Override
	public void edit_clasa_ID(Object o,String ID_nou) {
		// TODO Auto-generated method stub
		if (o instanceof Clasa ){
			Clasa copie = (Clasa) o;
			if (Centralizator.getInstance().get_clase().contains(copie)){
				int index =Centralizator.getInstance().get_clase().indexOf(copie);
				ArrayList<Clasa> clase = Centralizator.getInstance().get_clase();
				clase.get(index).set_idClasa(ID_nou);
				
			}
		}
		
	}

	//editare numelui unei materii dintr-o clasa ,pentru un profesor din centralizator
	@Override
	public void edit_materie_p_nume(Object o,Profesor p,String nou) {
		// TODO Auto-generated method stub
		if (o instanceof Materie){
			Materie copie = (Materie) o;
			if (Centralizator.getInstance().get_centralizator().containsKey(copie)){
				if(Centralizator.getInstance().get_centralizator().get(copie).values().contains(p)){
					
					Materie copie2=copie;
					Centralizator.getInstance().get_centralizator().get(copie).values().remove(p);
					copie.setNume(nou);
					p.set_mat(copie);
					Centralizator.getInstance().get_centralizator().get(copie).values().add(p);
					Hashtable<Clasa,Profesor> tabel= Centralizator.getInstance().get_centralizator().get(copie2);
					Centralizator.getInstance().get_centralizator().put(copie, tabel);
				}
			}
		}
	}
	
	//editarea numarului de ore al unei materii dintr-o clasa ,al unui profesor din centralizator
	@Override
	public void edit_materie_p_nrore(Object o, Profesor p,int nr_ore) {
		// TODO Auto-generated method stub
		if (o instanceof Materie){
			Materie copie = (Materie) o;
			if (Centralizator.getInstance().get_centralizator().containsKey(copie)){
				if(Centralizator.getInstance().get_centralizator().get(copie).values().contains(p)){
					Materie copie2=copie;
					Centralizator.getInstance().get_centralizator().get(copie).values().remove(p);
					copie.set_nrore(nr_ore);
					p.set_mat(copie);
					Centralizator.getInstance().get_centralizator().get(copie).values().add(p);
					Hashtable<Clasa,Profesor> tabel= Centralizator.getInstance().get_centralizator().get(copie2);
					Centralizator.getInstance().get_centralizator().put(copie, tabel);
				}
			}
		}
	}
	
	//editarea situatie la teza  a unei materii a unui profesor din centralizator
	@Override
	public void edit_materie_p_teza(Object o, Profesor p,boolean sit) {
		// TODO Auto-generated method stub
		if (o instanceof Materie){
			Materie copie = (Materie) o;
			if (Centralizator.getInstance().get_centralizator().containsKey(copie)){
				if(Centralizator.getInstance().get_centralizator().get(copie).values().contains(p)){
					Materie copie2=copie;
					Centralizator.getInstance().get_centralizator().get(copie).values().remove(p);
					copie.set_teza(sit);
					p.set_mat(copie);
					Centralizator.getInstance().get_centralizator().get(copie).values().add(p);
					Hashtable<Clasa,Profesor> tabel= Centralizator.getInstance().get_centralizator().get(copie2);
					Centralizator.getInstance().get_centralizator().put(copie, tabel);
				}
			}
		}
	}
	
	//Stergerea unei materii pentru un profesor
	@Override
	public void del_materie_p(Object o,Profesor p) {
		// TODO Auto-generated method stub
		if (o instanceof Materie){
			Materie copie = (Materie) o;
			if(p.get_mat()==copie){
				p.set_mat(null);
				Centralizator.getInstance().get_centralizator().keySet().remove(copie);
			}
	}
	}
	
	//adaugarea unei materii la un profesor
	@Override
	public void add_materie_p(Object o,Profesor p) {
		// TODO Auto-generated method stub
		if(o instanceof Materie){
			Materie copie = (Materie) o;
			
			if (Centralizator.getInstance().get_centralizator().containsKey(copie)){
				Centralizator.getInstance().get_centralizator().get(copie).values().remove(p);
				p.set_mat(copie);
				Centralizator.getInstance().get_centralizator().get(copie).values().add(p);
				
			}
			
			
		}
	}

	//adaugarea unei materii la o clasa
	@Override
	public void add_materie_c(Object o, Clasa y) {
		// TODO Auto-generated method stub
		if (o instanceof Materie){
			Materie mat= (Materie) o;
			if(Centralizator.getInstance().get_clase().contains(y)){
				ArrayList<Clasa> clase = Centralizator.getInstance().get_clase();
				int index = Centralizator.getInstance().get_clase().indexOf(y);
				ArrayList<Materie> materii= clase.get(index).get_materii();
				materii.add(mat);
				clase.get(index).set_materii(materii);
				Centralizator.getInstance().set_clase(clase);
			}
		}
	}

	//stergerea unei materii de la o clasa
	@Override
	public void del_materie_c(Object o, Clasa y) {
		// TODO Auto-generated method stub
		if (o instanceof Materie){
			Materie mat = (Materie) o;
			if(Centralizator.getInstance().get_clase().contains(y)){
				ArrayList<Clasa> clase=Centralizator.getInstance().get_clase();
				int index = Centralizator.getInstance().get_clase().indexOf(y);
				ArrayList<Materie> materii= clase.get(index).get_materii();
				materii.remove(mat);
				clase.get(index).set_materii(materii);
				Centralizator.getInstance().set_clase(clase);
			}
		}
	}

	//editarea numelui unei materii dintr-o clasa
	@Override
	public void edit_materie_c_nume(Object o,Clasa y,String nume){
		// TODO Auto-generated method stub
		if(o instanceof Materie){
			if (Centralizator.getInstance().get_clase().contains(y)){
				Materie mat = (Materie) o;
				Materie mat2=mat;
				ArrayList<Clasa> clase = Centralizator.getInstance().get_clase();
				int index = Centralizator.getInstance().get_clase().indexOf(y);
				ArrayList<Materie> materii= clase.get(index).get_materii();
				int index2 = materii.indexOf(mat);
				materii.get(index2).setNume(nume);
				clase.get(index).set_materii(materii);
				Centralizator.getInstance().set_clase(clase);
			}

		}
		}
	
	//editarea numarului de ore al unei materii dintr-o clasa
	@Override
	public void edit_materie_c_nrore(Object o, Clasa y,int nr_ore) {
		// TODO Auto-generated method stub
		if(o instanceof Materie){
			if (Centralizator.getInstance().get_clase().contains(y)){
				Materie mat = (Materie) o;
				Materie mat2=mat;
				ArrayList<Clasa> clase = Centralizator.getInstance().get_clase();
				int index = Centralizator.getInstance().get_clase().indexOf(y);
				ArrayList<Materie> materii= clase.get(index).get_materii();
				int index2 = materii.indexOf(mat);
				materii.get(index2).set_nrore(nr_ore);
				clase.get(index).set_materii(materii);
				Centralizator.getInstance().set_clase(clase);
			}

		}
	}
	
	//adaugarea unui elev la o clasa
	@Override
	public void add_elev_c(Object o, Clasa y) {
		// TODO Auto-generated method stub
		if (o instanceof Elev){
			Elev elev = (Elev) o;
			Clasa copie = y;
			if(Centralizator.getInstance().get_clase().contains(y)){
				ArrayList<Clasa> clase = Centralizator.getInstance().get_clase();
				int index = Centralizator.getInstance().get_clase().indexOf(y);
				clase.get(index).add_elev(elev);
				Centralizator.getInstance().set_clase(clase);
				//Centralizator.getInstance().save();
			}
		}
	}

	//stergerea unui elev dintr-o clasa
	@Override
	public void del_elev_c(Object o, Clasa y) {
		// TODO Auto-generated method stub
		if ( o instanceof Elev){
			Elev elev = (Elev) o;
			if(Centralizator.getInstance().get_clase().contains(y)){
				ArrayList<Clasa> clase = Centralizator.getInstance().get_clase();
				int index = Centralizator.getInstance().get_clase().indexOf(y);
				clase.get(index).del_elev(elev);
				Centralizator.getInstance().set_clase(clase);
			}
		}
	}

	//editarea numelui unui elev dintr-o clasa
	@Override
	public void edit_elev_c_nume(Object o, Clasa y,String nume) {
		// TODO Auto-generated method stub
		if (o instanceof Elev){
			Elev elev = (Elev) o;
			Clasa copie = y;
			if (Centralizator.getInstance().get_clase().contains(y)){
					ArrayList<Clasa> clase = Centralizator.getInstance().get_clase();
					int index = clase.indexOf(y);
					if (clase.get(index).get_elevi().contains(elev)){
						ArrayList<Elev> elevi = clase.get(index).get_elevi();
						int index2= elevi.indexOf(elev);
						elevi.get(index2).set_nume(nume);
						clase.get(index).set_elevi(elevi);
						Centralizator.getInstance().set_clase(clase);
					}
				}
			
		}
	}
	
	//editarea prenumelkui unui elev dintr-o clasa
	@Override
	public void edit_elev_c_prenume(Object o,Clasa y,String prenume){
		if (o instanceof Elev){
			Elev elev = (Elev) o;
			Clasa copie = y;
			if (Centralizator.getInstance().get_clase().contains(y)){
					ArrayList<Clasa> clase = Centralizator.getInstance().get_clase();
					int index = clase.indexOf(y);
					if (clase.get(index).get_elevi().contains(elev)){
						ArrayList<Elev> elevi = clase.get(index).get_elevi();
						int index2= elevi.indexOf(elev);
						elevi.get(index2).set_prenume(prenume);
						clase.get(index).set_elevi(elevi);
						Centralizator.getInstance().set_clase(clase);
					}
				}
			
		}
	}
	
	//Editarea datei de nastere a unjhuui elev dintr-o clasa
	@Override
	public void edit_elev_c_datanastere(Object o, Clasa y,Data data) {
		// TODO Auto-generated method stub
		if (o instanceof Elev){
			Elev elev = (Elev) o;
			Clasa copie = y;
			if (Centralizator.getInstance().get_clase().contains(y)){
					ArrayList<Clasa> clase = Centralizator.getInstance().get_clase();
					int index = clase.indexOf(y);
					if (clase.get(index).get_elevi().contains(elev)){
						ArrayList<Elev> elevi = clase.get(index).get_elevi();
						int index2= elevi.indexOf(elev);
						elevi.get(index2).set_data(data);
						clase.get(index).set_elevi(elevi);
						Centralizator.getInstance().set_clase(clase);
					}
				}
			
		}
		
	}
	
	//calcularea mediei generale a unui elev dintr-o clasa
	@Override
	public void calc_mg(Object o,Clasa y) {
		// TODO Auto-generated method stub
		if (o instanceof Elev){
			Elev elev=(Elev) o;
			int index = Centralizator.getInstance().get_clase().indexOf(y);
			if (y.get_elevi().contains(elev)){
				Hashtable<Materie,SituatieMaterieBaza> tabel =y.get_Catalog().get_catalog().get(elev);
				double suma=0;
				while (tabel.values().iterator().hasNext()){
					suma+=tabel.values().iterator().next().calc_medie_finala();
				}
				double medie = suma/tabel.keySet().size();
				System.out.println("Media generala a elevui\r\n" + elev.toString() + " este :\r\n" + medie);
			
			}
		}
	}
	
	
	//functie pentru afisarea informatiilor unui secretar
	public String toString(){
		return super.toString();
	}
	/*
	public Secretar getInstance(){
		return this;
	}*/
	/*public static void main(String[] args){
		Materie a = new Materie("Biologie",4,false);
		Materie fiz = new Materie("Fizica ",6,true);
		Materie mate = new Materie("Mate",5,false);
		Data data = new Data(15,12,1993);
		Profesor prof = new Profesor("vasile","tudor","Vasile","Ion","profeosr",mate);
		Elev tudor = new Elev("tudor","tudor","tudor","tudor","elev",data);
		Elev marius = new Elev("marius","marius","marius","marius","elev",data);
		Elev cosmin = new Elev("cosmin","cosmin","cosmin","cosmin","elev",data);
		Elev alexandra = new Elev("alexandra","alexandra","alexandra","alexandra","elev",data);
		Elev lucian = new Elev("lucian","lucian","lucian","lucian","elev",data);
		Elev alexandru = new Elev("alex","alex","alex","alex","elev",data);
		Secretar marin = new Secretar("marin","marin","marin","marin","secretar");
		Clasa unu= new Clasa();
		unu.set_idClasa("9B");
		unu.add_elev(tudor);
		unu.add_elev(marius);
		unu.add_elev(cosmin);
		
		SituatieMaterieBaza sit1 = new SituatieMaterieBaza();
		sit1.add_nota_sem1(4);
		sit1.add_nota_sem1(7);
		sit1.add_nota_sem1(6);
		sit1.add_nota_sem2(2);
		sit1.add_nota_sem2(6);
		sit1.add_nota_sem2(4);
		Absente ab1= new Absente();
		ab1.set_date(data);
		ab1.set_situatie("motivata");
		sit1.set_absente(new ArrayList());
		
		SituatieMaterieBaza sit2 = new SituatieMaterieBaza();
		sit2.add_nota_sem1(4);
		sit2.add_nota_sem1(5);
		sit2.add_nota_sem1(1);
		sit2.add_nota_sem2(4);
		sit2.add_nota_sem2(6);
		sit2.add_nota_sem2(8);
		sit2.set_absente(new ArrayList());
		
		sit1.set_nume_materie(mate);
		sit2.set_nume_materie(fiz);
		
		Hashtable<Materie,SituatieMaterieBaza> cat = new Hashtable<Materie, SituatieMaterieBaza>();
		cat.put(mate, sit1);
		cat.put(fiz, sit2);
		unu.add_elev_special(tudor, cat);
		
		SituatieMaterieBaza sit11 = new SituatieMaterieBaza();
		sit11.add_nota_sem1(4);
		sit11.add_nota_sem1(2);
		sit11.add_nota_sem1(5);
		sit11.add_nota_sem2(2);
		sit11.add_nota_sem2(6);
		sit11.add_nota_sem2(4);
		
		sit11.set_absente(new ArrayList());
		
		SituatieMaterieBaza sit22 = new SituatieMaterieBaza();
		sit22.add_nota_sem1(4);
		sit22.add_nota_sem1(5);
		sit22.add_nota_sem1(1);
		sit22.add_nota_sem2(4);
		sit22.add_nota_sem2(6);
		sit22.add_nota_sem2(8);
		sit22.set_absente(new ArrayList());
		
		sit11.set_nume_materie(mate);
		sit22.set_nume_materie(fiz);
		
		Hashtable<Materie,SituatieMaterieBaza> cat2 = new Hashtable<Materie, SituatieMaterieBaza>();
		cat2.put(mate, sit11);
		cat2.put(fiz, sit22);
		unu.add_elev_special(marius, cat);
		
		Clasa doi = new Clasa();
		doi.set_idClasa("Sefii");
		doi.add_elev(alexandra);
		
		ArrayList<Clasa> clase = new ArrayList<Clasa>();
		clase.add(unu);
		clase.add(doi);
		ArrayList<Utilizator> users = new ArrayList<Utilizator>();
		users.add(tudor);
		users.add(alexandru);
		users.add(cosmin);
		users.add(marius);
		users.add(alexandra);
		
		// Hashtable<Materie,Hashtable<Clasa,Profesor>>
		
		Centralizator.getInstance().set_clase(clase);
		Centralizator.getInstance().set_utilizatori(users);
		Hashtable<Clasa,Profesor> tabel = new Hashtable<Clasa,Profesor>();
		tabel.put(unu, prof);
		tabel.put(doi, prof);
		Hashtable<Materie,Hashtable<Clasa,Profesor>> tabel2= new Hashtable<Materie,Hashtable<Clasa,Profesor>>();
		tabel2.put(mate, tabel);
		//marin.del_clasa(doi);
		//marin.add_clasa(doi);ID_nou);
		//marin.add_elev_c(alexandru, unu);
		marin.edit_elev_c_nume(tudor, unu, "vasile");
		Centralizator.getInstance().set_centralizator(tabel2);
		System.out.println(Centralizator.getInstance().get_clase());
		//prof.add_note_sem2(tudor, unu, 1015515151, prof);
		//prof.inch_medie1(tudor, unu, prof.materie, prof);
		//prof.inch_medie2(tudor, unu, prof.materie, prof);
		//prof.list_clasa(unu);
		int index=Centralizator.getInstance().get_clase().indexOf(unu);
		//System.out.println(Centralizator.getInstance().get_clase().get(index).get_Catalog().get_catalog().get(tudor));
		//tudor.list_sitsc(unu);
		ArrayList<Materie> lista = new ArrayList<Materie>();
		lista.add(mate);
		lista.add(fiz);
		//prof.sortare("Prenume", unu);
		unu.set_materii(lista);
		
		
		
		
		
	}*/
	
}
