package liceu;

import java.io.*;
import java.util.*;

public class Centralizator implements Serializable{

	//am implementat aceasta clasa folosing Singleton Pattern
	private static final long serialVersionUID = 1L;

	private ArrayList<Utilizator> utilizatori;
	private ArrayList<Clasa> clase;
	private Hashtable<Materie,Hashtable<Clasa,Profesor>> centralizator;

	//<SingletonPattern>
	private static final Centralizator INSTANCE = new Centralizator();

	//Constructor implicit mai special(Special in s ensul ca adaug mereu un utilizator administrator
	//nu inseamna ca o sa apara de x ori acest utilizator dar am vrut sa pornesc cu un utilizator
	private Centralizator()  {
		utilizatori=new ArrayList<Utilizator>();
		clase=new ArrayList<Clasa>();
		centralizator=new Hashtable<Materie, Hashtable<Clasa,Profesor>>();
		this.utilizatori.add(new Utilizator("tudor","tudor","tudor","tudor","Administrator"));


	}

	public static Centralizator getInstance() {
		return INSTANCE;
	}
	//</SingletonPattern>

	//metode

	//existenta unui elev in centralizator
	public boolean elev_exista(String x){
		for(Utilizator user :this.utilizatori){
			if (user.get_numeutilizator().equals(x)){
				return true;
			}
			return false;
		}
		return false;
	}

	//metoda pentru afisarea elevilor unei clase intr-un vector de String-uri
	//folosesc aceasta metoda la interfata pentru a pune intr=un Jlist
	public String[] elevi_clasa(Clasa x){
		int index = Centralizator.getInstance().clase.indexOf(x);
		int contor=0;
		if (index >= 0){
			String [] s = new String[Centralizator.getInstance().get_clase().get(index).get_elevi().size()];
			if (Centralizator.getInstance().clase.contains(x)){

				for (Elev elev:Centralizator.getInstance().get_clase().get(index).get_elevi()){
					s[contor++]=elev.get_nume() + "  " + elev.get_prenume();
				}

			}
			return s;
		}
		else{
			String[] copie = {"Bedrule e un prost"};
			return copie;

		}
	}

	//metoda pentru afisarea materiilor unei clase intr-un vector de String-uri
	//folosesc aceasta metoda la interfata pentru a pune intr=un Jlist
	public String[] materii_clasa(Clasa x){

		int index = Centralizator.getInstance().clase.indexOf(x);
		// System.out.println("index: " +index);
		int contor = 0;
		if (index>=0){
			String[]s = new String [Centralizator.getInstance().get_clase().get(index).get_materii().size()];
			for (Materie materie : Centralizator.getInstance().get_clase().get(index).get_materii()){
				s[contor++]=materie.get_nume();
			}
			return s;
		}
		else{
			String[] copie = {"mere"};
			return copie;

		}

	}

	//metoda pentru salvarea listei claselor din centralizator intr-un vector de stringuri
	//pentru ca a folosi aceasta lista ulterior intr-un jlist
	public String[] get_listclase(){
		String[] s=new String[this.clase.size()];
		int contor=0;
		for (Clasa cls:this.clase){
			s[contor++]=cls.getidClasa();
		}
		return s;
	}
	//metoda pentru memorearea intr-un sir de stringuri a tututor utilizatorilor din clasa
	//centralizator...Voi folosi la clasa admin din pachetul grafic
	public String[] get_username(){
		String[] s=new String[this.utilizatori.size()];
		int contor=0;
		for (Utilizator x:this.utilizatori){
			s[contor++]=x.get_numeutilizator();
		}
		return s;
	}

	//Metoda pentru afisarea elevilor existenti in catalog
	//folosesc la interfata pentru anumite jListuri
	public String[] elevi_catalog(){
		String[] s = new String[Centralizator.getInstance().utilizatori.size()];
		int contor=0;

		for (Utilizator x : Centralizator.getInstance().utilizatori)
		{
			System.out.println(x.toString());
			if (x.get_rol().equals("Elev")){
				s[contor++]=x.get_nume() + " "  + x.get_prenume();
			}
		}
		return s;
	}


	//Aici am facut serializarea.Am facut o functie in interiorul Centralizatorului care se ocupa
	//de salvarea intr-un fisier a continutului centralizatorului pentru a avea mereu datele acolo
	public void save(){
		try  
		{  
			FileOutputStream fileOut = new FileOutputStream("Centralizator.ser");  
			ObjectOutputStream outStream = new ObjectOutputStream(fileOut);  
			outStream.writeObject(this);  
			outStream.close();  
			fileOut.close();  

		}catch(IOException i)  
		{  
			i.printStackTrace();  
		}  
	}


	public void set(Centralizator x){
		this.centralizator=x.get_centralizator();
		this.utilizatori=x.utilizatori;
		this.clase=x.clase;
	}

	//adaugarea unei clase in centralizator
	public void additem(Clasa c){
		clase.add(c);
	}


	//sterg o clasa din centralizator
	public void removeitem(Clasa c){
		clase.remove(c);
		centralizator.values().remove(c);
	}
	//getter si setter pentru utilizatori,clase,centralizatorul in sine
	public ArrayList<Utilizator> get_utilizatori(){
		return this.utilizatori;
	}

	public void set_utilizatori(ArrayList<Utilizator> a){
		this.utilizatori=a;
	}
	public ArrayList<Clasa> get_clase(){
		return this.clase;
	}
	public void set_clase(ArrayList<Clasa> a){
		this.clase=a;
	}

	public Hashtable<Materie,Hashtable<Clasa,Profesor>> get_centralizator(){
		return this.centralizator;
	}
	public void set_centralizator(Hashtable<Materie,Hashtable<Clasa,Profesor>> a){
		this.centralizator=a;
	}

	//metoda pentru a mai micsora din dimensiunea liniei
	public Hashtable<Materie,SituatieMaterieBaza> get_cat_clasa_elev(int index , Elev y){
		return  Centralizator.getInstance().get_clase().get(index).get_Catalog().get_catalog().get(y);
	}

	//metoda pentru a mai micsora din dimensiunea liniei
	public Hashtable<Elev,Hashtable<Materie,SituatieMaterieBaza>> get_cat_clasa(int index){
		return Centralizator.getInstance().get_clase().get(index).get_Catalog().get_catalog();
	}

	//adaugarea unui utilizator
	public void add(Utilizator x){
		this.getInstance().utilizatori.add(x);
	}

	//metoda toString() pentru aceasta clasa
	public String toString(){
		return this.centralizator + "\r\n" + this.utilizatori + "\r\n" + this.clase;
	}

}
