package liceu;

import java.io.*;

public class Data implements Serializable{
	
		/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
		private int day,year,month;
		
		//getter si setter pentru ziua de nastere
		public int get_day(){
			return day;
		}
		public 	void set_day(int x){
			day=x;
		}
		
		//getter si setter pentru luna de nastere
		public int get_month(){
			return this.month;
		}
		public 	void set_month(int x){
			this.month=x;
		}
		
		
		//getter si setter pentru anul nasterii
		public 	int get_year(){
			return this.year;
		}
		public 	void set_year(int x){
			year=x;
		}
		
		//constructorul implicit
		public Data(){
			this.day=0;
			this.month=0;
			this.year=0;
		}
		
		//Constructor special
		public Data(int zi,int luna ,int an){
			this.day=zi;
			this.month=luna;
			this.year=an;
		}
		
		//metoda toString() pentru aceasta clasa
		public String toString(){
			String s="";
			s=this.get_day()+" " +this.get_month()+" "+this.get_year()+" ";
			return s;
		}
	}

