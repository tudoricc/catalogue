package liceu;
import java.io.Serializable;
import java.util.*;
public class Catalog implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6988111829288157276L;
	private Hashtable<Elev,Hashtable<Materie,SituatieMaterieBaza>> catalogul = new Hashtable<Elev,Hashtable<Materie,SituatieMaterieBaza>>() ;
	
	//getter
	public Hashtable<Elev,Hashtable<Materie,SituatieMaterieBaza>> get_catalog(){
		return this.catalogul;
	}
	
	//constructorul de baza
	public Catalog(){
		this.catalogul=new Hashtable<Elev,Hashtable<Materie, SituatieMaterieBaza>>();
	}
	
	//setter
	public void set_catalog(Hashtable<Elev,Hashtable<Materie,SituatieMaterieBaza>> a){
		this.catalogul=a;
	}
	
	//afisarea pentru un elev a situatiei scolare
	public void afis_nume(Elev x){
		if (this.catalogul.containsKey(x)){
			System.out.println(this.catalogul.get(x).keySet());
			System.out.println(this.catalogul.get(x).values());
		}
	}
	//adaug un elev cu o singura materie si o singura materie de baza
	public void add_cat(Elev x,Materie y ,SituatieMaterieBaza z){
		Hashtable<Materie,SituatieMaterieBaza> a = new Hashtable<Materie,SituatieMaterieBaza>();
		a.put(y, z);
		this.get_catalog().put(x,a);
	}
	//adaug un elev cu mai multe materii si situatii materie de baza
	public void add_multi_cat(Elev x , Hashtable<Materie,SituatieMaterieBaza> a){
		this.get_catalog().put(x, a);
	}
	
	//metoda de afisare a unui catalog
	public String toString(){
		String s ="";
		for(Elev e : catalogul.keySet()){
			s+= e.toString()+"\r\n" + catalogul.get(e)+"\r\n";
		}
		return s;
	}
	
}
