package liceu;

import java.io.Serializable;
import java.util.ArrayList;

import liceu.SituatieMaterieBaza.Absenta;

class SituatieMaterieTeza extends SituatieMaterieBaza implements Serializable {
	private int nota_teza_sem1;
	private int nota_teza_sem2;
	
	//Constructor
	public SituatieMaterieTeza(){
		super();
		this.nota_teza_sem1=0;
		this.nota_teza_sem2=0;
	}
	
	//getter si setter nota teza sen 1
	int get_nota_teza_sem1(){
		return this.nota_teza_sem1;
	}
	void set_nota_teza_sem1(int x){
		this.nota_teza_sem1=x;
	}
	
	//getter si setter nota_teza_sem2
	int get_nota_teza_sem2(){
		return this.nota_teza_sem2;
		
	}
	void set_nota_teza_sem2(int x){
		this.nota_teza_sem2=x;
	}
	//media se calculeaza altfel la aceste materii
	
	//intai calculez pentru o materie pe primul semestru
	public float calc_medie_sem1(){
		float medie = this.calc_medie_sem1();
		float medie_f=(medie *3+ this.get_nota_teza_sem1())/4;
		medie_f=super.get_medie_sem1();
		return medie_f;
	}
	
	//apoi pe al doilea semestru
	public float calc_medie_sem2(){
		float medie = this.calc_medie_sem2();
		float medie_f=(medie *3+ this.get_nota_teza_sem2())/4;
		medie_f=super.get_medie_sem2();
		return medie_f;
	}
	
	//apoi medie finala a acelei materii
	public double calc_medie_finala(){
		double medie1,medie2;
		medie1=this.calc_medie_sem1();
		medie2=this.calc_medie_sem2();
		return (medie1+medie2)/2;
	}
	
	
	//metoda toString() pentru aceasta clasa
	public String toString(){
		return super.toString() + "\r\n nota teza semestrul 1" + this.get_nota_teza_sem1()+
				"\r\n Nota teza semestrul 2 "+ this.get_nota_teza_sem2();
	}
}
