package liceu;

import java.io.IOException;

interface IAdministrator extends ISecretar,IProfesor {
	//adaugarea unui nou utilizator
	boolean add(Object o,String id)throws IOException;

	//stergerea unui utilizator
	void delete(Object o)throws IOException;
	
	//listarea utilizatorilor
	void list();
	
}
