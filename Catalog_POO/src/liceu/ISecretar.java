package liceu;

interface ISecretar {
	
	//Adaugarea/Stergerea/Editarea unei clase
	void add_clasa(Object o);
	void del_clasa(Object o);
	void edit_clasa_ID(Object o,String ID_nou);
	

	//Adaugarea/stergerea/editarea unei materii pentru un profesor
	void edit_materie_p_nume(Object o,Profesor p,String nou);
	void edit_materie_p_nrore(Object o,Profesor p,int nr_ore);
	void edit_materie_p_teza(Object o,Profesor p,boolean sit);
	void del_materie_p(Object o,Profesor p);
	void add_materie_p(Object o,Profesor p);
	
	//Adaugarea/Stergerea/Editarea elevilor/materiilor unei clase
	void add_materie_c(Object o,Clasa y);
	void del_materie_c(Object o,Clasa y);
	void edit_materie_c_nume(Object o,Clasa y,String nume);
	void edit_materie_c_nrore(Object o,Clasa y,int nr_ore);
	void add_elev_c(Object o,Clasa y);
	void del_elev_c(Object o,Clasa y);
	void edit_elev_c_nume(Object o,Clasa y,String nume);
	void edit_elev_c_prenume(Object o,Clasa y,String prenume);
	void edit_elev_c_datanastere(Object o,Clasa y,Data data);
	
	//Calcularea mediei generale
	void calc_mg(Object o,Clasa y);
	
	
}
