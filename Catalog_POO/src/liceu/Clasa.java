package liceu;
import java.io.Serializable;
import java.util.*;
public class Clasa implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String idClasa;
	private ArrayList<Elev> elevi;
	private ArrayList<Materie> materii;
	private Catalog cat_clasa;
	
	public Clasa(){
		this.idClasa=new String();
		this.elevi=new ArrayList<Elev>();
		this.materii=new ArrayList<Materie>();
		this.cat_clasa=new Catalog();
	}
	
	//getter si setter pentru clasa
	public String getidClasa(){
		return this.idClasa;
	}
	public void set_idClasa(String a){
		this.idClasa=a;
	}
	
	//getter si stter pentru elevi
	public ArrayList<Elev> get_elevi(){
		return this.elevi;
	}
	
	public 	void set_elevi(ArrayList<Elev> a ){
		this.elevi=a;
	}
	
	//getter si setter pentru mateire
	public ArrayList<Materie> get_materii(){
		return this.materii;
	}
	public void set_materii(ArrayList<Materie> a){
		this.materii=a;
	}
	
	//getter si setter pentru Catalog
	public Catalog get_Catalog(){
		return this.cat_clasa;
	}
	public void set_cat(Catalog nou_cat){
		this.cat_clasa=nou_cat;
	}
	
	//metodele clasei "Clasa"
	
	//adaugarea unui elev la clasa noastra fara o situatie anume la materii
	public void add_elev(Elev a){
		this.elevi.add(a);
		//this.cat_clasa.get_catalog().put(a, null);
	}
	
	//adaugarea unui elev la clasa noastra cu o anumita situatie la materii
	public void add_elev_special(Elev a , Hashtable<Materie,SituatieMaterieBaza> x){
		this.cat_clasa.add_multi_cat(a, x);
	}
	
	//stergerea unui elev din clasa respectiva
	public void del_elev(Elev a){
		this.elevi.remove(a);
		this.cat_clasa.get_catalog().remove(a);
	}
	public String toString(){
		String s = "";
		s= "\r\n id-ul clasei este:" +this.idClasa +"\r\n lista elevilor: \r\n" +this.elevi.toString() +"\r\n \r\n materii: \r\n"+ this.materii.toString()
				+ "\r\n \r\n Catalogul: \r\n" + cat_clasa;
		return s;
	}
}
