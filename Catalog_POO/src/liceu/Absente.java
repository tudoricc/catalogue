package liceu;

public class Absente {
	
		private String situatie;
		private Data date;
		
		String get_situatie(){
			return this.situatie;
		}
		void set_situatie(String x){
			this.situatie=x;
		}
		
		Data get_date(){
			return this.date;
		}
		void set_date(Data a){
			this.date=a;
		}
		public String toString(){
			return this.get_date().toString() +" "+ this.get_situatie();
		}
	}

