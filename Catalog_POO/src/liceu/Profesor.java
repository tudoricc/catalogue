package liceu;


import java.io.Serializable;
import java.util.*;

public class Profesor extends Utilizator implements IProfesor,Serializable {
	private Materie materie;
	
	//Constructor
	public Profesor(Materie materie_initiala){
		this.materie=materie_initiala;
	}
	public Profesor(String username,String parola,String nume,String prenume,String rol,Materie x){
		this.materie=x;
		this.set_nume(nume);
		this.set_numeutilizator(username);
		this.set_parola(parola);
		this.set_prenume(prenume);
		this.set_rol(rol);
	}
	public Profesor(){
		super();
		Materie a = new Materie();
		materie=a;
	}
	public Profesor(Profesor x){
		this.set_mat(x.get_mat());
		this.set_nume(x.get_nume());
		this.set_numeutilizator(x.get_numeutilizator());
		this.set_parola(x.get_parola());
		this.set_prenume(x.get_prenume());
	}
	
	//getter si setter pentru materie
	public Materie get_mat (){
		return this.materie;
	}
	public void set_mat(Materie s){
		this.materie=s;
	}
	
	
	//metoda toString() pentru clasa profesor;
	public String toString(){
		String s = "";
		s=this.materie +"\r\n"+ super.toString();
		return s;
	}
	
	//afisarea continutului unei clase
	@Override
	public void list_clasa(Clasa x) {
		// TODO Auto-generated method stub
		int index = Centralizator.getInstance().get_clase().indexOf(x);
		for (Elev e :Centralizator.getInstance().get_clase().get(index).get_elevi()){
			System.out.println( e.toString()) ;
		}
	}
	//sortarea unei clase
	@Override
	public void sortare(Object o,Clasa x) {
		// TODO Auto-generated method stub
		if (o.equals("Nume")){
			ArrayList<Elev> copie = new ArrayList<Elev>();
			copie=x.get_elevi();
			class NameCompare implements Comparator<Elev>{

				@Override
				public int compare(Elev o1, Elev o2) {
					// TODO Auto-generated method stub
					String nume1=o1.get_nume();
					String nume2=o2.get_nume();
					return nume1.compareToIgnoreCase(nume2);
				}
			}
			Collections.sort(copie,new NameCompare());
			copie.toString();
		}
		else if (o.equals("Prenume")){
			ArrayList<Elev> copie = new ArrayList<Elev>();
			copie=x.get_elevi();
			class SurnameCompare implements Comparator<Elev>{

				@Override
				public int compare(Elev o1, Elev o2) {
					// TODO Auto-generated method stub
					String nume1=o1.get_prenume();
					String nume2=o2.get_prenume();
					return nume1.compareToIgnoreCase(nume2);
				}
			}
			Collections.sort(copie,new SurnameCompare());
			copie.toString();
		}
		else if (o.equals("CNP")){
			ArrayList<Elev> copie = new ArrayList<Elev>();
			copie=x.get_elevi();
			class CNPCompare implements Comparator<Elev>{

				@Override
				public int compare(Elev o1, Elev o2) {
					// TODO Auto-generated method stub
					String nume1=o1.get_cnp();
					String nume2=o2.get_cnp();
					return nume1.compareToIgnoreCase(nume2);
				}
			}
			Collections.sort(copie,new CNPCompare());
			copie.toString();
			
		}
		else if (o.equals ("Ziua nastere")){
			ArrayList<Elev> copie = new ArrayList<Elev>();
			copie=x.get_elevi();
			class DayCompare implements Comparator<Elev>{

				@Override
				public int compare(Elev o1, Elev o2) {
					// TODO Auto-generated method stub
					int nume1=o1.get_data().get_day();
					int nume2=o2.get_data().get_day();
					if (nume1<nume2){
						return -1;
						
					}
					else return 1;
				}
			}
			Collections.sort(copie,new DayCompare());
			copie.toString();
		}
		else if (o.equals ("Luna nastere")){
			ArrayList<Elev> copie = new ArrayList<Elev>();
			copie=x.get_elevi();
			class MonthCompare implements Comparator<Elev>{

				@Override
				public int compare(Elev o1, Elev o2) {
					// TODO Auto-generated method stub
					int nume1=o1.get_data().get_month();
					int nume2=o2.get_data().get_month();
					if (nume1<nume2){
						return -1;
						
					}
					else return 1;
				}
			}
			Collections.sort(copie,new MonthCompare());
			copie.toString();
		}
		else if(o.equals("Anul nasterii")){
			ArrayList<Elev> copie = new ArrayList<Elev>();
			copie=x.get_elevi();
			class YearCompare implements Comparator<Elev>{

				@Override
				public int compare(Elev o1, Elev o2) {
					// TODO Auto-generated method stub
					int nume1=o1.get_data().get_year();
					int nume2=o2.get_data().get_year();
					if (nume1<nume2){
						return -1;
						
					}
					else return 1;
				}
			}
			Collections.sort(copie,new YearCompare());
			copie.toString();
		}
		}
	
	//adaugarea notelor pe semestrul 1
	@Override
	public void add_note_sem1(Elev x,Clasa y, int nota,Profesor p) {
		// TODO Auto-generated method stub
		
		//deschidere fisier serializare 
		if (Centralizator.getInstance().get_clase().contains(y)){

			int index=Centralizator.getInstance().get_clase().indexOf(y);
			if (y.get_elevi().contains(x)){
				
				Centralizator.getInstance().get_cat_clasa_elev(index, x).get(p.get_mat()).add_nota_sem1(nota);
			}
			else{
				System.out.println("ai gresit ceva");
			}
		}
		//isalvare fisier
	}
	
	//adaugarea notelor pe semestrul 2
	@Override
	public	void add_note_sem2(Elev x ,Clasa y, int nota,Profesor p){
		if (Centralizator.getInstance().get_clase().contains(y)){
			if (y.get_elevi().contains(x)){
				int index = Centralizator.getInstance().get_clase().indexOf(y);
				Centralizator.getInstance().get_cat_clasa_elev(index, x).get(this.get_mat()).add_nota_sem2(nota);
			}
			else{
				System.out.println("ai gresit ceva");
			}
		}
	}
	
	
	//am facut metode speciale de incheiere a mediilor pentru profesor pentru ca 
	//m-am gandit ca administratorul nu stie materia ca parametru[profesorul stie]
	//si el poate incheia materia la o singura materie
	public void inch_medie_prof(Elev x,Clasa y ){
		// TODO Auto-generated method stub
		int index = Centralizator.getInstance().get_clase().indexOf(y);
		if (Centralizator.getInstance().get_cat_clasa(index).containsKey(x)){
			Centralizator.getInstance().get_cat_clasa_elev(index, x).get(this.get_mat()).set_medie_sem1();
			Centralizator.getInstance().get_cat_clasa_elev(index, x).get(this.get_mat()).set_medie_sem2();
			
		}
		
	}
	
	//incheiere medie pe semestrul 1 
	public void inch_medie1_prof(Elev x,Clasa y,Profesor p) {
		// TODO Auto-generated method stub
		int index = Centralizator.getInstance().get_clase().indexOf(y);
		if (Centralizator.getInstance().get_cat_clasa(index).containsKey(x)){
			Centralizator.getInstance().get_cat_clasa_elev(index, x).get(this.get_mat()).set_medie_sem1();
	}
	}

	//incheiere medie pe semestrul 2
	public void inch_medie2_prof(Elev x,Clasa y,Profesor p) {
		// TODO Auto-generated method stub
		int index = Centralizator.getInstance().get_clase().indexOf(y);
		if (Centralizator.getInstance().get_cat_clasa(index).containsKey(x)){
			Centralizator.getInstance().get_cat_clasa_elev(index, x).get(this.get_mat()).set_medie_sem2();
			
		}
		
	}
	
	
	//adaugarea de absente unui elev
	@Override
	public void add_abs(Elev x, Clasa y,Data date,Profesor p) {
		// TODO Auto-generated method stub
		if (y.get_elevi().contains(x)){
			if(Centralizator.getInstance().get_clase().contains(y)){
				ArrayList<Clasa> clase=Centralizator.getInstance().get_clase();

				int index = Centralizator.getInstance().get_clase().indexOf(y);
				
			
			}
			
		//	y.get_Catalog().get_catalog().
		}
		
	}
	
	@Override
	public void mod_abs(Clasa x, Elev y, Absente abs, String situatie,Profesor p) {
		// TODO Auto-generated method stub
		Absente absenta=new Absente();
		absenta.set_date(abs.get_date());
		absenta.set_situatie(situatie);
		int index1 = Centralizator.getInstance().get_clase().indexOf(x);
		if (Centralizator.getInstance().get_clase().get(index1).get_elevi().contains(y)){
			if (Centralizator.getInstance().get_clase().get(index1).get_Catalog().get_catalog().get(y).get(this.get_mat()).get_absente().contains(abs)){
				int index=Centralizator.getInstance().get_cat_clasa_elev(index1,y).get(this.get_mat()).get_absente().indexOf(abs);
				Centralizator.getInstance().get_cat_clasa_elev(index1, y).get(this.get_mat()).get_absente().get(index).set_date(absenta.get_date());
				Centralizator.getInstance().get_cat_clasa_elev(index1, y).get(this.get_mat()).get_absente().get(index).set_situatie(situatie);
			}
		}
	}
	
	@Override
	public void inch_medii(Elev x, Clasa y, Materie z,Profesor p) {
		// TODO Auto-generated method stub
		int index = Centralizator.getInstance().get_clase().indexOf(y);
		
		if (Centralizator.getInstance().get_clase().get(index).get_Catalog().get_catalog().containsKey(x)){
			Centralizator.getInstance().get_cat_clasa_elev(index, x).get(z).set_medie_sem1();
			Centralizator.getInstance().get_cat_clasa_elev(index,x).get(z).set_medie_sem2();
			
		}
		
	}
	@Override
	public void inch_medie1(Elev x, Clasa y, Materie z,Profesor p) {
		// TODO Auto-generated method stub
		int index=Centralizator.getInstance().get_clase().indexOf(y);
		if (Centralizator.getInstance().get_cat_clasa(index).containsKey(x)){
			Centralizator.getInstance().get_cat_clasa_elev(index, x).get(z).set_medie_sem1();		
			
		}
		
	}
	@Override
	public void inch_medie2(Elev x, Clasa y, Materie z,Profesor p) {
		// TODO Auto-generated method stub
		int index=Centralizator.getInstance().get_clase().indexOf(y);
		if (Centralizator.getInstance().get_cat_clasa(index).containsKey(x)){
			Centralizator.getInstance().get_cat_clasa_elev(index, x).get(this.get_mat()).set_medie_sem2();
			
		}
	}
	
	
	
}
