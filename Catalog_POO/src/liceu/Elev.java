package liceu;

import java.io.Serializable;

public class Elev extends Utilizator implements IElev,Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String CNP;
	private Data data_nastere;
	
	//constructori
	public Elev(){
		super();
		this.CNP=" ";
		this.data_nastere=new Data();
	}
	public Elev(Utilizator x){
		this.set_nume(x.get_nume());
		this.set_numeutilizator(x.get_numeutilizator());
		this.set_parola(x.get_parola());
		this.set_prenume(x.get_prenume());
		this.set_rol(x.get_rol());
		
	}
	 public Elev( String x , String y ,String z1,String z,String z2 , Data m){
		this.data_nastere=m;
		this.CNP=x;
		this.set_nume(y);
		this.set_numeutilizator(z);
		this.set_prenume(z1);
		this.set_parola(z2);
	}
	public Elev(String nume,String prenume,String username,String parola,String rol,String cnp,Data data){
		this.set_nume(nume);
		this.set_numeutilizator(username);
		this.set_parola(parola);
		this.set_rol(rol);
		this.CNP=cnp;
		this.data_nastere=data;
	
	}
	
	public Elev(Elev x) {
		// TODO Auto-generated constructor stub
		this.set_CNP(x.get_cnp());
		this.set_data(x.get_data());
		this.set_nume(x.get_nume());
		this.set_numeutilizator(x.get_numeutilizator());
		this.set_parola(x.get_parola());
		this.set_prenume(x.get_prenume());
	}
	
	
	//listarea situatiei scolarea a unui elev
	@Override
	public void list_sitsc(Object o) {
		// TODO Auto-generated method stub
		if (o instanceof Clasa){
			Clasa x = (Clasa) o;
			int index = Centralizator.getInstance().get_clase().indexOf(x);
			for (Elev e : Centralizator.getInstance().get_clase().get(index).get_elevi()){
				if (e.equals(this)){
					System.out.println(Centralizator.getInstance().get_clase().get(index).get_Catalog().get_catalog().get(e));
				}
			}
		}
	}
	
	//getter si setter pentru CNP
	public String get_cnp(){
		return this.CNP;
	}
	public void set_CNP(String x){
		this.CNP=x;
	}
	
	//getter si setter pentru data
	public Data get_data(){
		return this.data_nastere;
	}
	public 	void set_data(Data x){
		this.data_nastere=x;
	}
	
	
	//metoda toString() pentru aceasta clasa
	public String toString(){
		return super.toString() + "\r\n CNP: " + this.get_cnp() +"\r\n" + "Data nastere: " + this.get_data().toString() + "\r\n";
	}
	public Elev getInstance(){
		return this;
	}

}
