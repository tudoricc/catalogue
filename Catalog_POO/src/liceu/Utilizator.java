package liceu;

import java.io.Serializable;

public class Utilizator implements Serializable {
	private String nume_utilizator;
	private String parola;
	private String nume;
	private String prenume;
	private String rol;

	//Constructori
	public Utilizator(){
		this.nume_utilizator="";
		this.nume=this.nume_utilizator=" ";
		this.parola=this.nume=" ";
		this.prenume=this.rol=" ";
	}

	public  Utilizator(String user , String parola , String nume , String prenume){
		this.nume_utilizator=user;
		this.parola=parola;
		this.nume=nume;
		this.prenume=prenume;
	}
	public  Utilizator(String user , String parola , String nume , String prenume,String rol){
		this.nume_utilizator=user;
		this.parola=parola;
		this.nume=nume;
		this.prenume=prenume;
		this.rol=rol;
	}

	//metoda de obtinere a numelui utilizatorului
	public String get_numeutilizator(){
		return this.nume_utilizator;
	}

	//metoda de initiere a numelui utilizatorului
	public void set_numeutilizator(String s){
		this.nume_utilizator=s;
	}

	//metodata de obtinere a parolei
	public  String get_parola(){
		return this.parola;
	}

	//metoda de initiere a parolei
	public void set_parola(String s){
		this.parola=s;
	}

	//metoda de obtinere a numelui
	public String get_nume(){
		return this.nume;
	}

	//metoda de initiere a numelui
	public void set_nume(String s){
		this.nume=s;
	}

	//metoda de obtinere a prenumelui
	public String get_prenume(){
		return this.prenume;
	}

	//metoda de initiere a prenumelui
	public void set_prenume(String s){
		this.prenume=s;
	}
	public String get_rol(){
		return this.rol;
	}

	//metoda de initiere a numelui utilizatorului
	public void set_rol(String s){
		this.rol=s;
	}

	//metoda toString pentru aceasta clasa
	public String toString(){
		String s = "";
		s="Nume Utilizator: "+ this.nume_utilizator +"\r\n Parola: "+
				this.parola +"\r\n Nume: " + this.nume  +"\r\n Prenume: " +this.prenume+"\r\n Rol:"+this.rol;

		return s;
	}
}
