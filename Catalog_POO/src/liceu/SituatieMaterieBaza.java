package liceu;
import java.io.Serializable;
import java.util.*;

public class SituatieMaterieBaza implements Serializable {
	//clasa interna absenta
	class Absenta{
		
		private String situatie;
		private Data date;
		
		String get_situatie(){
			return this.situatie;
		}
		void set_situatie(String x){
			this.situatie=x;
		}
		
		Data get_date(){
			return this.date;
		}
		void set_date(Data a){
			this.date=a;
		}
	
	}
	private Materie nume_materie;
	private ArrayList<Integer> note_sem1;
	private ArrayList<Integer> note_sem2;
	private float medie_sem1,medie_sem2;
	private ArrayList<Absenta> absente;
	
	//Constructori
	public SituatieMaterieBaza(){
		nume_materie = new Materie();
		note_sem1=new ArrayList<Integer>();
		note_sem2=new ArrayList<Integer>();
		absente=new ArrayList<Absenta>();
	}
	SituatieMaterieBaza(Materie x, ArrayList<Integer> n1, ArrayList<Integer> n2, int m1,int m2,ArrayList<Absenta> abs){
		this.nume_materie=x;
		this.note_sem1=n1;
		this.note_sem2=n2;
		this.medie_sem1=m1;
		this.medie_sem2=m2;
		this.absente=abs;
	}
	
	//getter si setter pentru nume materie
	public Materie get_materie(){
		return this.nume_materie
				;
	}
	public void set_nume_materie(Materie a){
		this.nume_materie=a;
	}
	
	//getter si setter pentru note_sem1
	public 	ArrayList<Integer> get_note_sem1(){
		return this.note_sem1;
	}
	public void set_note_sem1(ArrayList<Integer> a){
		this.note_sem1=a;
	}
	
	//getter si setter pentru note sem2
	public 	ArrayList<Integer> get_note_sem2(){
		return this.note_sem2;
	}
	public 	void set_note_sem2(ArrayList<Integer> a){
		this.note_sem2=a;
	}
	
	//getter si setter pentru medie_sem1 si sem2
	public 	float get_medie_sem1(){
			return this.medie_sem1;
		}
	public void set_medie_sem1(){
			this.medie_sem1=calc_medie_sem1();
		}
	
	public float get_medie_sem2(){
			return this.medie_sem2;
		}
	
	public void set_medie_sem2(){
			this.medie_sem2=calc_medie_sem2();
		}
	//getter si setter pentru lista cu absente
	public ArrayList<Absenta> get_absente(){
			return this.absente;
		}
	public void set_absente(ArrayList<Absenta> a){
			this.absente=a;
		}
	
	
	//metode pentru clasa noastra
	public float calc_medie_sem1(){
		int suma = 0;
		float medie;
		for (int i:this.note_sem1){
			suma+=i;
		}
		medie=(float)suma/this.note_sem1.size();
		return medie ;
				
	}
	public float calc_medie_sem2(){
		int suma = 0;
		float medie;
		for( int i=0;i<this.get_note_sem2().size();i++){
			suma= suma + this.note_sem2.get(i);
		}
		medie = (float)suma/this.get_note_sem2().size();
		return medie;
	}
	public double calc_medie_finala(){
		return (this.calc_medie_sem1() + this.calc_medie_sem2())/2;
	}
	public void add_nota_sem1(int x){
		this.get_note_sem1().add(x);
	}
	
	public void add_nota_sem2(int x){
		this.get_note_sem2().add(x);
	}
	
	public void add_absenta(Absenta absenta){
		this.get_absente().add(absenta);
		int index = this.get_absente().indexOf(absenta);
		this.get_absente().get(index).set_situatie("Nedeterminat");
		System.out.println("Ai pus o absenta");
	}
	
	public void motiv_absenta(Absenta x){
		if (this.get_absente().contains(x)){
			int index = this.get_absente().indexOf(x);
			this.get_absente().get(index).set_situatie("Motivat");
	}
		System.out.println("Ai motivat absenta");
	}
	
	
	//metoda toString() pentru aceasta clasa
	public String toString(){
		String s="";
		s="\r\nNumele materiei: "+ this.get_materie().toString() + "\r\nNote semestrul1: "+this.get_note_sem1().toString();
		s=s+"	medie sem1:  "+ this.get_medie_sem1()+"\r\nNote semestrul2:"+ this.get_note_sem2() + "	medie sem2: " +this.get_medie_sem2()+"\r\n" ;
		s=s+this.get_absente().toString() ;
		return s;
		
	}
}