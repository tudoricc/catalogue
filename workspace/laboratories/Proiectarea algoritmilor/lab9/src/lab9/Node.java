package lab9;

public class Node {
	public int _id;
	public int value;
	public boolean _visited = false;
	public Node parent = null;

	public Node (int id ,int value){
		this._id=id;
		this.value = value;
	}
}
